﻿using Inc.Analitic.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inc.Analitic.Wpf.Models
{
    public class ClientForView
    {
        public Client Client { get; set; }

        public Employee Employee { get; set; }

        public ClientStatus NewStatus { get; set; }

        public Employee NewEmployee { get; set; }
    }
}
