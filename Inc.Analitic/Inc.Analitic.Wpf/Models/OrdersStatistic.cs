﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inc.Analitic.Wpf.Models
{
    public class OrdersStatistic : PropertyChangedBase
    {
        public string Name { get; set; }

        public int Amount { get; set; }

    }
}
