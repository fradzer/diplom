﻿using Caliburn.Micro;

namespace PerfomanceTest.Models
{
    class TestInformation : PropertyChangedBase
    {
        private Settings settings;
        private int totalRequests = 0;
        private int successRequests = 0;
        private int failedRequests = 0;
        private long bandwidth = 0;
        private int totalErrors = 0;
        private long minResponse = 0;
        private long maxResponse = 0;
        private long averageResponse = 0;
        private double progress = 0;
        private int progressMax = 100;
        private int requestsPerSecond = 0;
        private int successRequestsPerSecond = 0;
        private int failedRequestsPerSecond = 0;


        public double Progress
        {
            get { return progress; }
            set
            {
                progress = value;
                NotifyOfPropertyChange();
            }
        }

        public int SuccessRequests
        {
            get { return successRequests; }
            set
            {
                successRequests = value;
                NotifyOfPropertyChange();
            }
        }

        public int FailedRequests
        {
            get { return failedRequests; }
            set
            {
                failedRequests = value;
                NotifyOfPropertyChange();
            }
        }

        public long Bandwidth
        {
            get { return bandwidth; }
            set
            {
                bandwidth = value;
                NotifyOfPropertyChange();
            }
        }

        public int TotalErrors
        {
            get { return totalErrors; }
            set
            {
                totalErrors = value;
                NotifyOfPropertyChange();
            }
        }

        public long MinResponse
        {
            get { return minResponse; }
            set
            {
                minResponse = value;
                NotifyOfPropertyChange();
            }
        }

        public long MaxResponse
        {
            get { return maxResponse; }
            set
            {
                maxResponse = value;
                NotifyOfPropertyChange();
            }
        }

        public int TotalRequests
        {
            get { return totalRequests; }
            set
            {
                totalRequests = value;
                NotifyOfPropertyChange();
            }
        }

        public long AverageResponse
        {
            get { return averageResponse; }
            set
            {
                averageResponse = value;
                NotifyOfPropertyChange();
            }
        }

        public int RequestsPerSecond
        {
            get { return requestsPerSecond; }
            set
            {
                requestsPerSecond = value;
                NotifyOfPropertyChange();
            }
        }

        public int SuccessRequestsPerSecond
        {
            get { return successRequestsPerSecond; }
            set
            {
                successRequestsPerSecond = value;
                NotifyOfPropertyChange();
            }
        }

        public int FailedRequestsPerSecond
        {
            get { return failedRequestsPerSecond; }
            set
            {
                failedRequestsPerSecond = value;
                NotifyOfPropertyChange();
            }
        }

        public int ProgressMax
        {
            get { return progressMax; }
        }


        public Settings Settings
        {
            get { return settings; }
        }


        public TestInformation(Settings settings)
        {
            this.settings = settings;
        }

    }
}
