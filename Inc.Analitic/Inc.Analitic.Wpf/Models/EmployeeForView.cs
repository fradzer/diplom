﻿using Inc.Analitic.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inc.Analitic.Wpf.Models
{
    public class EmployeeForView : Employee
    {
        public int ClientCount { get; set; }

        public bool IsNotAdmin { get; set; }
    }
}
