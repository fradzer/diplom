﻿namespace PerfomanceTest.Models
{
    public class Settings
    {
        public string Url { get; set; }
        public int TreadsCount { get; set; }
        public int RequestsCount { get; set; }
        public int Duration { get; set; }
    }
}
