﻿using Caliburn.Micro;
using Inc.Analitic.Data.Repositories;
using Inc.Analitic.Data.Repositories.Interfaces;
using Inc.Analitic.Data.Services;
using Inc.Analitic.Data.Services.Interfaces;
using Inc.Analitic.Wpf.ViewModels;
using PerfomanceTest.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inc.Analitic.Wpf
{
    class Bootstrapper : BootstrapperBase
    {
        private SimpleContainer _container = new SimpleContainer();

        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, System.Windows.StartupEventArgs e)
        {
            DisplayRootViewFor<AuthorizationViewModel>();
        }

        protected override void Configure()
        {
            _container.Singleton<IWindowManager, WindowManager>();
            _container.Singleton<IEventAggregator, EventAggregator>();
            _container.PerRequest<AuthorizationViewModel>();
            _container.PerRequest<MainViewModel>();
            _container.PerRequest<ClientsViewModel>();
            _container.PerRequest<ClientViewModel>();
            _container.PerRequest<OrdersChartViewModel>();
            _container.PerRequest<AdminClientsViewModel>();
            _container.PerRequest<AdminEmployeesViewModel>();
            _container.PerRequest<AdminPricesViewModel>();
            _container.PerRequest<SettingsViewModel>();

            // Repositories
            _container.PerRequest<IAlgorithmRepository, AlgorithmRepository>();
            _container.PerRequest<IEmployeeRepository, EmployeeRepository>();
            _container.PerRequest<IClientRepository, ClientRepository>();
            _container.PerRequest<IOrderRepository, OrderRepository>();

            // Services
            _container.PerRequest<IEmployeeService, EmployeeService>();
            _container.PerRequest<IClientService, ClientService>();
            _container.PerRequest<IOrderService, OrderService>();
            _container.PerRequest<IAlgorithmService, AlgorithmService>();

            base.Configure();
        }

        protected override object GetInstance(Type service, string key)
        {
            var instance = _container.GetInstance(service, key);
            if (instance != null)
                return instance;

            throw new InvalidOperationException("Could not locate any instances.");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }
    }
}
