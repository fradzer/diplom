IF db_id('IncAnalitic') IS NULL 
begin

CREATE DATABASE IncAnalitic

CREATE TABLE IncAnalitic.dbo.Algorithms ( 
 Id int IDENTITY(1,1),  
 [Name] nvarchar(50),
 Price Decimal,
 PRIMARY KEY (Id)

) 

CREATE TABLE IncAnalitic.dbo.EmployeeStatuses (
 Id int IDENTITY(1,1),  
 [Name] nvarchar(10),
  PRIMARY KEY (Id)
) 

CREATE TABLE IncAnalitic.dbo.Employees (
 Id int IDENTITY(1,1),  
 [Name] nvarchar(50),
 HashPassword nvarchar(300),
 [Status] int,
  PRIMARY KEY (Id),
      FOREIGN KEY ([Status]) REFERENCES EmployeeStatuses(Id)  
) 

CREATE TABLE IncAnalitic.dbo.ClientStatuses (
 Id int IDENTITY(1,1),  
 [Name] nvarchar(10),
  PRIMARY KEY (Id)
) 

CREATE TABLE IncAnalitic.dbo.Clients (
 Id int IDENTITY(1,1),  
 CompanyName nvarchar(50),
 [Status] int,
  Capital Decimal,
  Site nvarchar(50),
  EmployeeId int,
  PRIMARY KEY (Id),
      FOREIGN KEY ([Status]) REFERENCES ClientStatuses(Id),
      FOREIGN KEY (EmployeeId) REFERENCES Employees(Id)
) 

CREATE TABLE IncAnalitic.dbo.Orders (
 Id int IDENTITY(1,1),
 CreateDate datetimeoffset,
 EmployeeId int,
 ClientId int,
 CPA int,
 CPO int,
 CPS int,
 CPL int,
 CPI int,
 Price Decimal,
  PRIMARY KEY (Id),
      FOREIGN KEY (ClientId) REFERENCES Clients(Id),
      FOREIGN KEY (EmployeeId) REFERENCES Employees(Id)
) 
end