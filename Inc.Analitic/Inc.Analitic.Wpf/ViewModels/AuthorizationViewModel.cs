﻿using Caliburn.Micro;
using Inc.Analitic.Data.Services;
using Inc.Analitic.Data.Services.Interfaces;
using System.Windows;

namespace Inc.Analitic.Wpf.ViewModels
{
    class AuthorizationViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly IEmployeeService employeeService;
        private string name;
        private string password;

        public System.Action CloseAction { get; set; }
        public System.Action ShowError { get; set; }


        public AuthorizationViewModel(IWindowManager windowManager, IEmployeeService employeeService)
            : base()
        {
            this.windowManager = windowManager;
            this.employeeService = employeeService;
        }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                NotifyOfPropertyChange();
            }
        }

        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                NotifyOfPropertyChange();
            }
        }

        public void Authorize()
        {
            var successAuth = employeeService.SignIn(Name, Password);
            if (successAuth)
            {
                var MainViewModel = IoC.Get<MainViewModel>();
                windowManager.ShowWindow(MainViewModel);
                CloseAction();
            }
            else
            {
                ShowError();
            }
        }
    }
}
