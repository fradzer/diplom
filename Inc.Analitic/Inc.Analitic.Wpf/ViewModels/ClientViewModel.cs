﻿using Caliburn.Micro;
using Inc.Analitic.Business.Models;
using Inc.Analitic.Data.Services.Interfaces;
using Inc.Analitic.Data.Session;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Inc.Analitic.Data.Utils;

namespace Inc.Analitic.Wpf.ViewModels
{
    public class ClientViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly IOrderService orderService;
        private readonly IAlgorithmService algorithmService;
        private readonly IClientService clientService;
        private Client client;
        private Order newOrder;
        private ObservableCollection<Order> orders;
        private string pathToFile;

        public System.Action ShowError { get; set; }
        public System.Action ShowSuccess { get; set; }
        public ClientViewModel(IWindowManager windowManager, IOrderService orderService, IAlgorithmService algorithmService, IClientService clientService)
        {
            InitOrder();
            this.windowManager = windowManager;
            this.orderService = orderService;
            this.algorithmService = algorithmService;
            this.clientService = clientService;
        }

        private void InitOrder()
        {
            NewOrder = new Order() { CreateDate = DateTimeOffset.Now };
        }

        public string PathToFile
        {
            get
            {
                return pathToFile;
            }
            set
            {
                pathToFile = value;
                NotifyOfPropertyChange();
            }
        }

        public Client Client
        {
            get { return client; }
            set
            {
                client = value;
                NotifyOfPropertyChange();

                if (Orders == null)
                {
                    InitOrders();
                }
            }
        }

        private void InitOrders()
        {
            Orders = new ObservableCollection<Order>(orderService.GetAllByClientId(client.Id));
        }

        public Order NewOrder
        {
            get { return newOrder; }
            set
            {
                newOrder = value;
                NotifyOfPropertyChange();
            }
        }

        public ObservableCollection<Order> Orders
        {
            get
            {
                return orders;
            }
            set
            {
                orders = value;
                NotifyOfPropertyChange();

            }
        }

        public void Add()
        {
            if (Orders.All(x => x.CreateDate.Date != newOrder.CreateDate.Date))
            {
                var price = algorithmService.GetPrice(newOrder.CPA, newOrder.CPO, newOrder.CPS, newOrder.CPL, newOrder.CPI);
                client.Capital += price;
                clientService.Update(client);
                newOrder.EmployeeId = CurrentEmployee.Id;
                newOrder.ClientId = client.Id;
                newOrder.Price = price;
                orderService.Insert(NewOrder);
                InitOrder();
                Notify();
                InitOrders();
                ShowSuccess();
            }
            else
            {
                ShowError();
            }
        }

        public void RemoveOrder(Order order)
        {
            orderService.Delete(order.Id);
            Orders.Remove(order);
            client.Capital -= order.Price;
            clientService.Update(client);
            Notify();
        }

        public void Dowloand()
        {
            if (string.IsNullOrWhiteSpace(PathToFile))
            {
                ShowError();
            }
            else
            {
                List<Order> ordersFromJson;
                try
                {
                    ordersFromJson = ReadUtil.ReadJsonWithOrders(PathToFile);
                    if (Orders.Any(x => ordersFromJson.Any(y => y.CreateDate == x.CreateDate)))
                    {
                        ShowError();
                    }
                    else
                    {
                        ordersFromJson.ForEach(x => x.ClientId = Client.Id);
                        orderService.Insert(ordersFromJson);
                        ordersFromJson.ForEach(x => client.Capital += x.Price);
                        clientService.Update(client);
                        InitOrders();
                        Notify();
                        ShowSuccess();
                    }
                }
                catch(Exception ex)
                {
                    ShowError();
                }
            }
        }

        public void Charts()
        {

            var OrdersVM = IoC.Get<OrdersChartViewModel>();
            OrdersVM.Orders = Orders.ToList();
            windowManager.ShowWindow(OrdersVM);
        }

        private void Notify()
        {
            NotifyOfPropertyChange("Orders");
            NotifyOfPropertyChange("Client");
        }
    }
}
