﻿using Caliburn.Micro;
using Inc.Analitic.Business.Models;
using Inc.Analitic.Data.Services.Interfaces;
using Inc.Analitic.Data.Session;
using System.Collections.Generic;
using System.Linq;

namespace Inc.Analitic.Wpf.ViewModels
{
    public class ClientsViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly IClientService clientService;
        private List<Client> clients;

        public List<Client> Clients
        {
            get
            {
                return clients;
            }
            set
            {
                clients = value;
                NotifyOfPropertyChange();
            }
        }

        public ClientsViewModel(IWindowManager windowManager, IClientService clientService)
        {
            this.windowManager = windowManager;
            this.clientService = clientService;

            Clients = clientService.GetAllActiveByEmployeeId(CurrentEmployee.Id).ToList();
        }

        public void Client(Client client)
        {
            var clientViewModel = IoC.Get<ClientViewModel>();
            clientViewModel.Client = client;
            windowManager.ShowWindow(clientViewModel);
        }
    }
}
