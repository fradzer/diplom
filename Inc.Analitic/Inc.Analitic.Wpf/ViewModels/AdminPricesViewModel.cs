﻿using Caliburn.Micro;
using Inc.Analitic.Business.Models;
using Inc.Analitic.Data.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Inc.Analitic.Wpf.ViewModels
{
    public class AdminPricesViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly IAlgorithmService algorithmService;
        private Algorithm CpaPrice;
        private Algorithm CpoPrice;
        private Algorithm CpsPrice;
        private Algorithm CplPrice;
        private Algorithm CpiPrice;

        public AdminPricesViewModel(IWindowManager windowManager, IAlgorithmService algorithmService)
        {
            this.windowManager = windowManager;
            this.algorithmService = algorithmService;
            InitPrices();

        }

        public Algorithm CPAPrice
        {
            get { return CpaPrice; }
            set
            {
                CpaPrice = value;
                NotifyOfPropertyChange();
            }
        }

        public Algorithm CPOPrice
        {
            get { return CpoPrice; }
            set
            {
                CpoPrice = value;
                NotifyOfPropertyChange();
            }
        }

        public Algorithm CPSPrice
        {
            get { return CpsPrice; }
            set
            {
                CpsPrice = value;
                NotifyOfPropertyChange();
            }
        }

        public Algorithm CPLPrice
        {
            get { return CplPrice; }
            set
            {
                CplPrice = value;
                NotifyOfPropertyChange();
            }
        }

        public Algorithm CPIPrice
        {
            get { return CpiPrice; }
            set
            {
                CpiPrice = value;
                NotifyOfPropertyChange();
            }
        }
        
        public void Save()
        {
            var algorithms = new List<Algorithm> { CPAPrice, CPOPrice, CPSPrice, CPLPrice, CPIPrice };
            algorithmService.Update(algorithms);
            Execute.OnUIThread(() => MessageBox.Show("Обновлено", "Цены"));

        }

        private void InitPrices()
        {
            var prices = algorithmService.GetAll();

            CPAPrice = prices.FirstOrDefault(x => x.Name.Equals("CPA", StringComparison.CurrentCultureIgnoreCase));
            CPOPrice = prices.FirstOrDefault(x => x.Name.Equals("CPO", StringComparison.CurrentCultureIgnoreCase));
            CPSPrice = prices.FirstOrDefault(x => x.Name.Equals("CPS", StringComparison.CurrentCultureIgnoreCase));
            CPLPrice = prices.FirstOrDefault(x => x.Name.Equals("CPL", StringComparison.CurrentCultureIgnoreCase));
            CPIPrice = prices.FirstOrDefault(x => x.Name.Equals("CPI", StringComparison.CurrentCultureIgnoreCase));
        }
    }
}
