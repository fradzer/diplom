﻿using Caliburn.Micro;
using PerfomanceTest.Models;

namespace PerfomanceTest.ViewModels
{
    class SettingsViewModel : PropertyChangedBase
    {
        public Settings Settings { get; set; }
        public const string DefaultUrl = "http://vk.com";
        public int TreadsMin { get; private set; }
        public int TreadsMax { get; private set; }
        public int RequestsMin { get; private set; }
        public int RequestsMax { get; private set; }
        public int DurationMin { get; private set; }
        public int DurationMax { get; private set; }

        private readonly IWindowManager windowManager = new WindowManager();


        public SettingsViewModel()
            : base()
        {
            TreadsMin = 1;
            TreadsMax = 15;
            RequestsMin = 1;
            RequestsMax = 50;
            DurationMin = 1;
            DurationMax = 999;

            Settings = new Settings
            {
                Url = DefaultUrl,
                TreadsCount = 2,
                RequestsCount = 20,
                Duration = 10
            };
        }
        public string Url
        {
            get { return Settings.Url; }
            set
            {
                Settings.Url = value;
                NotifyOfPropertyChange();
            }
        }

        public int TreadsCount
        {
            get { return Settings.TreadsCount; }
            set
            {
                Settings.TreadsCount = value;
                NotifyOfPropertyChange();
            }
        }

        public int RequestsCount
        {
            get { return Settings.RequestsCount; }
            set
            {
                Settings.RequestsCount = value;
                NotifyOfPropertyChange();
            }
        }

        public int Duration
        {
            get { return Settings.Duration; }
            set
            {
                Settings.Duration = value;
                NotifyOfPropertyChange();
            }
        }

        public void Run()
        {
            windowManager.ShowDialog(new TestViewModel(Settings));
        }
    }
}
