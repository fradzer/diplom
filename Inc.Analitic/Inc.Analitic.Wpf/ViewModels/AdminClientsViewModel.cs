﻿using Caliburn.Micro;
using Inc.Analitic.Business.Models;
using Inc.Analitic.Data.Services.Interfaces;
using Inc.Analitic.Wpf.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inc.Analitic.Wpf.ViewModels
{
    class AdminClientsViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly IEmployeeService employeeService;
        private readonly IClientService clientService;
        private string newClientName;
        private string newClientSite;
        private Employee newClientEmployee;

        public IEnumerable<ClientStatus> ClientStatuses { get; set; }
        public IEnumerable<Employee> Employees { get; set; }
        private ObservableCollection<ClientForView> clients;

        public System.Action ShowError { get; set; }
        public System.Action ShowSuccess { get; set; }
        public System.Action ShowUpdateSuccess { get; set; }

        public AdminClientsViewModel(IWindowManager windowManager, IEmployeeService employeeService, IClientService clientService)
        {
            this.windowManager = windowManager;
            this.employeeService = employeeService;
            this.clientService = clientService;
            SetEmployees();
            SetStatuses();
            SetClients();
        }

        public ObservableCollection<ClientForView> Clients
        {
            get
            {
                return clients;
            }
            set
            {
                clients = value;
                NotifyOfPropertyChange();

            }
        }

        public string NewClientName
        {
            get
            {
                return newClientName;
            }
            set
            {
                newClientName = value;
                NotifyOfPropertyChange();
            }
        }

        public string NewClientSite
        {
            get
            {
                return newClientSite;
            }
            set
            {

                newClientSite = value;
                NotifyOfPropertyChange();
            }
        }

        public Employee NewClientEmployee
        {
            get
            {
                return newClientEmployee;
            }
            set
            {
                newClientEmployee = value;
                NotifyOfPropertyChange();
            }
        }


        public void AddNewClient()
        {
            if (clientService.TryAddClient(NewClientName, NewClientSite, NewClientEmployee.Id))
            {
                Clients.Add(new ClientForView
                {
                    Client = clientService.GetAll()
                    .FirstOrDefault(x => x.CompanyName.Equals(NewClientName, StringComparison.CurrentCultureIgnoreCase)),
                    Employee = employeeService.GetAll().FirstOrDefault(x => x.Id == NewClientEmployee.Id)
                });
                NotifyOfPropertyChange("Clients");
                ShowSuccess();
            }
            else
            {
                ShowError();
            }
        }

        public void Update(ClientForView client)
        {
            client.Client.Status = client.NewStatus == 0 ? client.Client.Status : client.NewStatus;
            client.Client.EmployeeId = client.NewEmployee == null ? client.Client.EmployeeId : client.NewEmployee.Id;
            client.Employee = client.NewEmployee ?? client.Employee;
            Clients.Remove(client);
            Clients.Add(client);
            clientService.Update(client.Client);
            NotifyOfPropertyChange("Clients");
            ShowUpdateSuccess();

        }

        private void SetClients()
        {
            var allClients = clientService.GetAll().Select(x =>
            new ClientForView {
                Client = x,
                Employee = employeeService.GetAll().FirstOrDefault(y => y.Id == x.EmployeeId)
            });

            Clients = new ObservableCollection<ClientForView>(allClients);
        }

        private void SetEmployees()
        {
            var employees = employeeService.GetAll();

            Employees = employees;
        }

        private void SetStatuses()
        {
            ClientStatuses = Enum.GetValues(typeof(ClientStatus)).Cast<ClientStatus>();
        }

    }
}
