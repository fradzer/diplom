﻿using Caliburn.Micro;
using PerfomanceTest.ViewModels;

namespace Inc.Analitic.Wpf.ViewModels
{
    public class MainViewModel: Screen
    {
        private readonly IWindowManager windowManager;

        public MainViewModel(IWindowManager windowManager)
        {
            this.windowManager = windowManager;
        }

        public void AdminClients()
        {
            var ClientsViewModel = IoC.Get<AdminClientsViewModel>();
            windowManager.ShowWindow(ClientsViewModel);
        }

        public void AdminEmployees()
        {
            var EmployeesViewModel = IoC.Get<AdminEmployeesViewModel>();
            windowManager.ShowWindow(EmployeesViewModel);
        }

        public void AdminPrices()
        {
            var PricesViewModel = IoC.Get<AdminPricesViewModel>();
            windowManager.ShowWindow(PricesViewModel);
        }

        public void Clients()
        {
            var ClientsViewModel = IoC.Get<ClientsViewModel>();
            windowManager.ShowWindow(ClientsViewModel);
        }

        public void PerfomanceSettings()
        {
            var settingsViewModel = IoC.Get<SettingsViewModel>();
            windowManager.ShowWindow(settingsViewModel);
        }

        public void Exit()
        {
            System.Windows.Application.Current.Shutdown();
        }
    }
}
