﻿using Caliburn.Micro;
using PerfomanceTest.Models;
using System.Collections.ObjectModel;
using System.Windows;

namespace PerfomanceTest.ViewModels
{
    class TestViewModel : PropertyChangedBase
    {
        public TestInformation TestInfo { get; private set; }
        public Testing Test { get; private set; }

        public TestViewModel(Settings settings)
            : base()
        {
            TestInfo = new TestInformation(settings);
            Test = new Testing(TestInfo, new Collection<RequestsStatistic> {
                new RequestsStatistic {Name = "Success" },
                new RequestsStatistic {Name= "Failed" }
                }
            );
            Test.TestFinished += Test_TestFinished;

            Test.Start();
        }

        public void Cancel()
        {
            Test.Cancel();
        }

        private void Test_TestFinished()
        {
            Execute.OnUIThread(() => MessageBox.Show("Finished", "Finished"));
        }
    }
}
