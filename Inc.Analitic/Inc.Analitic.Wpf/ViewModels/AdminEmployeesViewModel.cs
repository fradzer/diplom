﻿using Caliburn.Micro;
using Inc.Analitic.Business.Models;
using Inc.Analitic.Data.Services.Interfaces;
using Inc.Analitic.Wpf.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inc.Analitic.Wpf.ViewModels
{
    public class AdminEmployeesViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly IEmployeeService employeeService;
        private readonly IClientService clientService;
        private string newEmployeeName;
        private string newEmployeePassword;
        private EmployeeStatus newEmployeeStatus;

        public IEnumerable<EmployeeStatus> EmployeeStatuses { get; set; }
        private ObservableCollection<EmployeeForView> employees;

        public System.Action ShowError { get; set; }
        public System.Action ShowSuccess { get; set; }
        public System.Action ShowDeleteError { get; set; }
        public System.Action HiddenDeleteError { get; set; }

        public AdminEmployeesViewModel(IWindowManager windowManager, IEmployeeService employeeService, IClientService clientService)
        {
            this.windowManager = windowManager;
            this.employeeService = employeeService;
            this.clientService = clientService;
            SetStatuses();
            SetEmployees();
        }

        public ObservableCollection<EmployeeForView> Employees
        {
            get
            {
                return employees;
            }
            set
            {
                employees = value;
                NotifyOfPropertyChange();

            }
        }

        public string NewEmployeeName
        {
            get
            {
                return newEmployeeName;
            }
            set
            {
                newEmployeeName = value;
                NotifyOfPropertyChange();
            }
        }

        public string NewEmployeePassword
        {
            get
            {
                return newEmployeePassword;
            }
            set
            {
                newEmployeePassword = value;
                NotifyOfPropertyChange();
            }
        }

        public EmployeeStatus NewEmployeeStatus
        {
            get
            {
                return newEmployeeStatus;
            }
            set
            {
                newEmployeeStatus = value;
                NotifyOfPropertyChange();
            }
        }

        public void AddNewEmployee()
        {
            if (employeeService.TryAddEmployee(NewEmployeeName, NewEmployeePassword, NewEmployeeStatus))
            {
                Employees.Add(EmployeeConverter(employeeService.GetAll()
                    .FirstOrDefault(x => x.Name.Equals(NewEmployeeName, StringComparison.CurrentCultureIgnoreCase))));
                NotifyOfPropertyChange("Employees");
                ShowSuccess();
            }
            else
            {
                ShowError();
            }
        }

        public void DeleteEmployee(EmployeeForView employee)
        {
            if (employeeService.TryDeleteEmployee(employee))
            {
                Employees.Remove(employee);
                NotifyOfPropertyChange("Employees");
                HiddenDeleteError();
            }
            else
            {
                ShowDeleteError();
            }
        }

        private void SetEmployees()
        {
            var allEmployees = employeeService.GetAll()
                .Select(x => EmployeeConverter(x));

            Employees = new ObservableCollection<EmployeeForView>(allEmployees);
        }

        private EmployeeForView EmployeeConverter(Employee x)
        {
            return new EmployeeForView()
            {
                Id = x.Id,
                Name = x.Name,
                Status = x.Status,
                IsNotAdmin = x.Status != EmployeeStatus.Admin,
                ClientCount = clientService.GetAllByEmployeeId(x.Id).Count()
            };
        }
        private void SetStatuses()
        {
            EmployeeStatuses = Enum.GetValues(typeof(EmployeeStatus)).Cast<EmployeeStatus>();
        }

    }
}
