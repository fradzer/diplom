﻿using Caliburn.Micro;
using Inc.Analitic.Business.Models;
using Inc.Analitic.Data.Services.Interfaces;
using Inc.Analitic.Data.Utils;
using Inc.Analitic.Wpf.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inc.Analitic.Wpf.ViewModels
{
    public class OrdersChartViewModel : Screen
    {
        private readonly IOrderService orderService;
        private List<KeyValuePair<string, int>> chartList;
        private List<KeyValuePair<string, int>> lineChartList;
        private List<Order> orders;

        public List<Order> Orders
        {
            get
            {
                return orders;
            }
            set
            {
                orders = value;
                InitCharts();
            }
        }

        public OrdersChartViewModel(IOrderService orderService)
        {
            this.orderService = orderService;
        }

        private void InitCharts()
        {
            var valueList = new List<KeyValuePair<string, int>>
            {
                new KeyValuePair<string, int>("CPA", Orders.Sum(x => x.CPA)),
                new KeyValuePair<string, int>("CPO", Orders.Sum(x => x.CPO)),
                new KeyValuePair<string, int>("CPS", Orders.Sum(x => x.CPS)),
                new KeyValuePair<string, int>("CPL", Orders.Sum(x => x.CPL)),
                new KeyValuePair<string, int>("CPI", Orders.Sum(x => x.CPI))
            };

            ChartList = valueList;

            var valueListByOrder = OrderUtil.GetParamsByOrders(orders, "CPA");
            LineChartList = valueListByOrder;
        }

        public List<KeyValuePair<string, int>> ChartList
        {
            get
            {
                return chartList;
            }
            set
            {
                chartList = value;
                NotifyOfPropertyChange();
            }
        }

        public List<KeyValuePair<string, int>> LineChartList
        {
            get
            {
                return lineChartList;
            }
            set
            {
                lineChartList = value;
                NotifyOfPropertyChange();
            }
        }

        public void UpdateChart(string model)
        {
            var valueListByOrder = OrderUtil.GetParamsByOrders(orders, model);
            LineChartList = valueListByOrder;
        }
    }
}
