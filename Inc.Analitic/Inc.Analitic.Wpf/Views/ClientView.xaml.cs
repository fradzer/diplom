﻿using System.Windows;
using System.Windows.Input;
using System.Text.RegularExpressions;
using System;
using System.Windows.Media;

namespace Inc.Analitic.Wpf.Views
{
    /// <summary>
    /// Interaction logic for ClientView.xaml
    /// </summary>
    public partial class ClientView : Window
    {
        public ClientView()
        {
            InitializeComponent();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void BtnFileOpen_Click(object sender, RoutedEventArgs e)
        {
            var fileDialog = new System.Windows.Forms.OpenFileDialog();
            fileDialog.Filter = "Json (*.json) | *.json;";
            var result = fileDialog.ShowDialog();
            switch (result)
            {
                case System.Windows.Forms.DialogResult.OK:
                    var file = fileDialog.FileName;
                    TxtFile.Text = file;
                    Dowloand.IsEnabled = true;
                    break;
                case System.Windows.Forms.DialogResult.Cancel:
                default:
                    TxtFile.Text = null;
                    break;
            }
            InitMessage(sender, e);
        }

        private void AddActions()
        {
            if (((dynamic)DataContext).ShowError == null)
            {
                ((dynamic)DataContext).ShowError = new Action(ShowError);
            }
            if (((dynamic)DataContext).ShowSuccess == null)
            {
                ((dynamic)DataContext).ShowSuccess = new Action(ShowSuccess);
            }

        }

        private void ShowError()
        {
            ErrorMessage.Visibility = Visibility.Visible;
            SuccessMessage.Visibility = Visibility.Hidden;
        }
        private void ShowSuccess()
        {
            SuccessMessage.Visibility = Visibility.Visible;
            ErrorMessage.Visibility = Visibility.Hidden;
        }

        private void InitMessage(object sender, RoutedEventArgs e)
        {
            AddActions();
        }
    }
}
