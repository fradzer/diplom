﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Inc.Analitic.Wpf.Views
{
    /// <summary>
    /// Interaction logic for AdminEmployeesView.xaml
    /// </summary>
    public partial class AdminEmployeesView : Window
    {
        public AdminEmployeesView()
        {
            InitializeComponent();
        }

        private void TxtChanged(object sender, RoutedEventArgs e)
        {
            if (NewEmployeeName.Text.Length > 0 && NewEmployeePassword.Text.Length > 0 && NewEmployeeStatus.SelectedValue != null)
            {
                AddNewEmployee.IsEnabled = true;
            }
            else
            {
                AddNewEmployee.IsEnabled = false;
            }
            AddActions();
        }

        private void AddActions()
        {
            if (((dynamic)DataContext).ShowError == null)
            {
                ((dynamic)DataContext).ShowError = new Action(ShowError);
            }

            if (((dynamic)DataContext).ShowSuccess == null)
            {
                ((dynamic)DataContext).ShowSuccess = new Action(ShowSuccess);
            }

            if (((dynamic)DataContext).ShowDeleteError == null)
            {
                ((dynamic)DataContext).ShowDeleteError = new Action(ShowDeleteError);
            }

            if (((dynamic)DataContext).HiddenDeleteError == null)
            {
                ((dynamic)DataContext).HiddenDeleteError = new Action(HiddenDeleteError);
            }
        }

        private void ShowError()
        {
            ErrorMessage.Visibility = Visibility.Visible;
            SuccessMessage.Visibility = Visibility.Hidden;
        }

        private void ShowDeleteError()
        {
            DeleteError.Visibility = Visibility.Visible;
        }

        private void HiddenDeleteError()
        {
            DeleteError.Visibility = Visibility.Hidden;
        }

        private void ShowSuccess()
        {
            SuccessMessage.Visibility = Visibility.Visible;
            ErrorMessage.Visibility = Visibility.Hidden;
        }

        private void InitErrorMessage(object sender, RoutedEventArgs e)
        {
            AddActions();
        }
    }
}
