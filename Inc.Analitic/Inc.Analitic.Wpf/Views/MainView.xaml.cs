﻿using Inc.Analitic.Data.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Inc.Analitic.Wpf.Views
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : Window
    {
        public MainView()
        {
            InitializeComponent();
            CheckRole();
        }

        private void CheckRole()
        {
            if(!CurrentEmployee.IsAdmin())
            {
                AdminMenu.IsEditable = false;
                AdminMenu.IsHitTestVisible = false;
                AdminMenu.Focusable = false;
            }
        }
    }
}
