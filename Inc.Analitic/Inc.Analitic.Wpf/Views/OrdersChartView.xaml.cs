﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Inc.Analitic.Wpf.Views
{
    /// <summary>
    /// Interaction logic for OrdersChart.xaml
    /// </summary>
    public partial class OrdersChartView : Window
    {
        public OrdersChartView()
        {
            InitializeComponent();
        }

        private void CPO_Click(object sender, RoutedEventArgs e)
        {
            areaItem.Title = "CPO";
        }

        private void CPS_Click(object sender, RoutedEventArgs e)
        {
            areaItem.Title = "CPS";
        }

        private void CPL_Click(object sender, RoutedEventArgs e)
        {
            areaItem.Title = "CPL";
        }

        private void CPI_Click(object sender, RoutedEventArgs e)
        {
            areaItem.Title = "CPI";
        }

        private void CPA_Click(object sender, RoutedEventArgs e)
        {
            areaItem.Title = "CPA";
        }

        private void Capital_Click(object sender, RoutedEventArgs e)
        {
            areaItem.Title = "Капитал (бел.руб)";
        }
    }
}
