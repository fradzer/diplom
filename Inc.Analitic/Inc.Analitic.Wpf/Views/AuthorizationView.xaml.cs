﻿using Inc.Analitic.Wpf.ViewModels;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Inc.Analitic.Wpf.Views
{
    /// <summary>
    /// Interaction logic for AuthorizationView.xaml
    /// </summary>
    public partial class AuthorizationView : Window
    {
        public AuthorizationView()
        {
            InitializeComponent();
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (DataContext != null)
            {
                var password = Password.Password;
                ((dynamic)DataContext).Password = password;
            }
        }

        private void TxtChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox_PasswordChanged(sender, e);
            if (Password.Password.Length > 0 && Login.Text.Length > 0)
            {
                Authorize.IsEnabled = true;
            }
            else
            {
                Authorize.IsEnabled = false;
            }
            AddActions();
        }

        private void AddActions()
        {
            if (((dynamic)DataContext).CloseAction == null)
            {
                ((dynamic)DataContext).CloseAction = new Action(Close);
            }


            if (((dynamic)DataContext).ShowError == null)
            {
                ((dynamic)DataContext).ShowError = new Action(ShowError);
            }
        }

        private void ShowError()
        {
            Error.Visibility = Visibility.Visible;
            Password.BorderBrush = Brushes.Red;
        }
    }
}
