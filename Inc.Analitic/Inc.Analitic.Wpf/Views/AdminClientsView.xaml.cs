﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Inc.Analitic.Wpf.Views
{
    /// <summary>
    /// Interaction logic for AdminClientsView.xaml
    /// </summary>
    public partial class AdminClientsView : Window
    {
        public AdminClientsView()
        {
            InitializeComponent();
        }


        private void TxtChanged(object sender, RoutedEventArgs e)
        {
            if (NewClientName.Text.Length > 0 && NewClientSite.Text.Length > 0 && NewClientEmployee.SelectedValue != null)
            {
                AddNewClient.IsEnabled = true;
            }
            else
            {
                AddNewClient.IsEnabled = false;
            }
        }

        private void AddActions()
        {
            if (((dynamic)DataContext).ShowError == null)
            {
                ((dynamic)DataContext).ShowError = new Action(ShowError);
            }

            if (((dynamic)DataContext).ShowSuccess == null)
            {
                ((dynamic)DataContext).ShowSuccess = new Action(ShowSuccess);
            }
            if (((dynamic)DataContext).ShowUpdateSuccess == null)
            {
                ((dynamic)DataContext).ShowUpdateSuccess = new Action(ShowUpdateSuccess);
            }
        }

        private void ShowUpdateSuccess()
        {
            UpdateSuccess.Visibility = Visibility.Visible;
        }

        private void ShowError()
        {
            ErrorMessage.Visibility = Visibility.Visible;
            SuccessMessage.Visibility = Visibility.Hidden;
        }
        private void ShowSuccess()
        {
            SuccessMessage.Visibility = Visibility.Visible;
            ErrorMessage.Visibility = Visibility.Hidden;
        }

        private void InitMessage(object sender, RoutedEventArgs e)
        {
            AddActions();
        }
    }
}
