﻿using Caliburn.Micro;
using PerfomanceTest.ViewModels;
using System.Windows;

namespace PerfomanceTest
{
    public class AppBootstrapper : BootstrapperBase
    {
        public AppBootstrapper()
        {
            Initialize();            
        }
        
        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<SettingsViewModel>();
        }
    }
}