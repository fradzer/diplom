﻿using Caliburn.Micro;

namespace PerfomanceTest.Models
{
    class RequestsStatistic : PropertyChangedBase
    {
        private string name;
        private int amount;

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                NotifyOfPropertyChange();
            }
        }

        public int Amount
        {
            get { return amount; }
            set
            {
                amount = value;
                NotifyOfPropertyChange();
            }
        }

    }
}
