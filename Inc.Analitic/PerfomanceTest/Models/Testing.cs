﻿using Caliburn.Micro;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace PerfomanceTest.Models
{
    class Testing : PropertyChangedBase
    {
        private TestInformation testInfo;
        private Collection<RequestsStatistic> chartCollection;
        private int requestsInOneSecond = 0;
        private int successRequestsInOneSecond = 0;
        private int failedRequestsInOneSecond = 0;

        public delegate void TestFinishedEventHandler();
        public event TestFinishedEventHandler TestFinished;
        private CancellationTokenSource cancelTS;

        public Collection<RequestsStatistic> ChartCollection
        {
            get { return chartCollection; }
            set
            {
                chartCollection = value;
                NotifyOfPropertyChange();
            }
        }

        public Testing(TestInformation testInfo, Collection<RequestsStatistic> chartCollection)
        {
            this.testInfo = testInfo;
            this.chartCollection = chartCollection;
            cancelTS = new CancellationTokenSource();
        }

        public void Cancel()
        {
            cancelTS.Cancel();
        }

        public void Start()
        {
            int seconds = 0;
            Task.Run(() =>
            {
                while (!cancelTS.IsCancellationRequested)
                {
                    testInfo.Progress = (testInfo.ProgressMax / (double)testInfo.Settings.Duration) * seconds;

                    Thread.Sleep(1000);
                    culculationPerSecond();
                    seconds++;
                }
                testInfo.Progress = testInfo.ProgressMax;
                TestFinished();

            }, cancelTS.Token);

            Parallel.For(0, testInfo.Settings.TreadsCount, (i) => SendRequests());
        }

        private async void SendRequests()
        {
            Task[] tasks;
            var ct = cancelTS.Token;

            tasks = Enumerable.Range(0, testInfo.Settings.RequestsCount)
                          .Select(i => Task.Run(() => GenerateRequestAsync(testInfo.Settings.Url, ct), ct)).ToArray();

            cancelTS.CancelAfter(testInfo.Settings.Duration * 1000);

            await Task.WhenAll(tasks);

        }

        private async Task GenerateRequestAsync(string url, CancellationToken ct)
        {
            HttpClient client =
                new HttpClient();
            while (!ct.IsCancellationRequested)
            {
                try
                {
                    ct.ThrowIfCancellationRequested();
                    Stopwatch watch = Stopwatch.StartNew();
                    var responseBytes = await client.GetByteArrayAsync(url);
                    watch.Stop();
                    if (testInfo.MinResponse > watch.ElapsedMilliseconds)
                    {
                        testInfo.MinResponse = watch.ElapsedMilliseconds;
                    }
                    if (testInfo.MaxResponse < watch.ElapsedMilliseconds)
                    {
                        testInfo.MaxResponse = watch.ElapsedMilliseconds;
                    }
                    if (testInfo.AverageResponse == 0)
                    {
                        testInfo.AverageResponse = watch.ElapsedMilliseconds;
                    }
                    else
                    {
                        testInfo.AverageResponse = (watch.ElapsedMilliseconds + testInfo.AverageResponse) / 2;
                    }
                    testInfo.SuccessRequests++;
                    successRequestsInOneSecond++;

                    ChartCollection[0].Amount++;

                    testInfo.Bandwidth = responseBytes.Length / watch.ElapsedMilliseconds;
                }
                catch (Exception)
                {
                    ChartCollection[1].Amount++;
                    failedRequestsInOneSecond++;
                    testInfo.TotalErrors++;
                }
                finally
                {
                    ChartCollection = ChartCollection;
                    requestsInOneSecond++;
                    testInfo.TotalRequests++;
                }
            }
        }

        private void culculationPerSecond()
        {
            testInfo.RequestsPerSecond = (testInfo.RequestsPerSecond + requestsInOneSecond) / 2;
            testInfo.SuccessRequestsPerSecond = (testInfo.SuccessRequestsPerSecond + successRequestsInOneSecond) / 2;
            testInfo.FailedRequestsPerSecond = (testInfo.FailedRequestsPerSecond + failedRequestsInOneSecond) / 2;
            requestsInOneSecond = 0;
            successRequestsInOneSecond = 0;
            failedRequestsInOneSecond = 0;
        }
    }
}
