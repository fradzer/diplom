﻿using Inc.Analitic.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inc.Analitic.Data.Utils
{
    public class OrderUtil
    {
        public static List<KeyValuePair<string, int>> GetParamsByOrders(List<Order> orders, string model)
        {
            var result = new List<KeyValuePair<string, int>>();

            switch (model)
            {
                case "CPA":
                    {
                        result.AddRange(
                            orders.OrderBy(x => x.CreateDate)
                            .Select(x => new KeyValuePair<string, int>(
                                x.CreateDate.Date.ToShortDateString(),
                                orders.Where(y => y.CreateDate <= x.CreateDate).Sum(y => y.CPA)
                            )));
                    }
                    break;
                case "CPI":
                    {
                        result.AddRange(
                            orders.OrderBy(x => x.CreateDate)
                            .Select(x => new KeyValuePair<string, int>(
                                x.CreateDate.Date.ToShortDateString(),
                                orders.Where(y => y.CreateDate <= x.CreateDate).Sum(y => y.CPI)
                            )));
                    }
                    break;
                case "CPL":
                    {
                        result.AddRange(
                            orders.OrderBy(x => x.CreateDate)
                            .Select(x => new KeyValuePair<string, int>(
                                x.CreateDate.Date.ToShortDateString(),
                                orders.Where(y => y.CreateDate <= x.CreateDate).Sum(y => y.CPL)
                            )));
                    }
                    break;
                case "CPO":
                    {
                        result.AddRange(
                            orders.OrderBy(x => x.CreateDate)
                            .Select(x => new KeyValuePair<string, int>(
                                x.CreateDate.Date.ToShortDateString(),
                                orders.Where(y => y.CreateDate <= x.CreateDate).Sum(y => y.CPO)
                            )));
                    }
                    break;
                case "CPS":
                    {
                        result.AddRange(
                            orders.OrderBy(x => x.CreateDate)
                            .Select(x => new KeyValuePair<string, int>(
                                x.CreateDate.Date.ToShortDateString(),
                                orders.Where(y => y.CreateDate <= x.CreateDate).Sum(y => y.CPS)
                            )));
                    }
                    break;
                case "Capital":
                    {
                        result.AddRange(
                            orders.OrderBy(x => x.CreateDate)
                            .Select(x => new KeyValuePair<string, int>(
                                x.CreateDate.Date.ToShortDateString(),
                                orders.Where(y => y.CreateDate <= x.CreateDate).Sum(y => (int)y.Price)
                            )));
                    }
                    break;
            }
            return result;
        }
    }
}
