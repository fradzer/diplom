﻿using Inc.Analitic.Business.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inc.Analitic.Data.Utils
{
    public class ReadUtil
    {
        public static List<Order> ReadJsonWithOrders(string path)
        {
            var fileData = File.ReadAllText(path);
            var orders = JsonConvert.DeserializeObject<List<Order>>(fileData);
            return orders;
        }
    }
}
