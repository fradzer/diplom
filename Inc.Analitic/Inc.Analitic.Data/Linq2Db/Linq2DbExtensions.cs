﻿using LinqToDB;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Inc.Analitic.Data.Linq2Db
{
    public static class Linq2DbExtensions
    {
        [Sql.Expression("{0} BETWEEN {1} AND {2}", PreferServerSide = true)]
        public static bool Between<T>(this T x, T low, T high)
            where T : IComparable<T>
        {
            // x >= low && x <= high
            return x.CompareTo(low) >= 0 && x.CompareTo(high) <= 0;
        }

        [Sql.Function("", ServerSideOnly = true, Configuration = "SqlServer")]
        [Sql.Function("julianday", ServerSideOnly = true, Configuration = "SQLite")]
        public static long JulianDays(this DateTimeOffset dt)
        {
            return dt.Ticks;
        }

        [Sql.Extension(typeof(FullTextSearchExtension), PreferServerSide = true, IsPredicate = true)]
        public static bool Matches(this string field, string match)
        {
            return field.Contains(match);
        }

        [Sql.Extension("dbo.STRING_AGG({expr})", PreferServerSide = true, IsAggregate = true, Configuration = "SqlServer")]
        [Sql.Extension("group_concat({expr},',')", PreferServerSide = true, IsAggregate = true, Configuration = "SQLite")]
        public static string Concatenate<T>(this IGrouping<int, T> collection, [ExprParameter] Expression<Func<T, string>> expr)
        {
            var func = expr.Compile();
            return string.Join(",", collection.Select(i => func(i)));
        }

        public class FullTextSearchExtension : Sql.IExtensionCallBuilder
        {
            public void Build(Sql.ISqExtensionBuilder builder)
            {
                if (builder.Mapping.ConfigurationID == "SQLite")
                {
                    builder.Expression = "{field} LIKE {match}";
                    var field = builder.GetExpression("field");
                    var match = builder.GetValue<string>("match");
                    builder.AddParameter("field", field);
                    builder.AddParameter("match", "%" + match + "%");
                }
                else if (builder.Mapping.ConfigurationID.StartsWith("SqlServer"))
                {
                    builder.Expression = "CONTAINS({field}, {match})";
                    var field = builder.GetExpression("field");
                    var match = builder.GetExpression("match");
                    builder.AddParameter("field", field);
                    builder.AddParameter("match", match);
                }
                else
                {
                    throw new NotSupportedException($"Data provider {builder.Mapping.ConfigurationID} is not supported.");
                }
            }
        }
    }

}
