﻿using Inc.Analitic.Business.Models;
using LinqToDB;
using LinqToDB.Data;
using System.Linq;

namespace Inc.Analitic.Data.Linq2Db
{
    class DbIncAnalitic: DataConnection
    {
        public DbIncAnalitic() : base("IncAnalitic") { }

        public ITable<Algorithm> Algorithms { get; set; }

        public ITable<Client> Clients { get; set; }

        public ITable<Employee> Employees { get; set; }

        public ITable<Order> Orders { get; set; }
    }
}
