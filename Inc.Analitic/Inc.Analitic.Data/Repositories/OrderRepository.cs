﻿using Inc.Analitic.Business.Models;
using Inc.Analitic.Data.Linq2Db;
using Inc.Analitic.Data.Repositories.Interfaces;
using LinqToDB;
using System.Collections.Generic;
using System.Linq;

namespace Inc.Analitic.Data.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        public void Delete(int id)
        {
            using (var db = new DbIncAnalitic())
            {
                db.GetTable<Order>()
                  .Where(p => p.Id == id)
                  .Delete();
            }
        }

        public IEnumerable<Order> GetAll()
        {
            using (var db = new DbIncAnalitic())
            {
                return db.GetTable<Order>().ToList();
            }
        }

        public void Insert(Order order)
        {
            using (var db = new DbIncAnalitic())
            {
                db.Insert(order);
            }
        }

        public void Insert(List<Order> orders)
        {
            using (var db = new DbIncAnalitic())
            {
                db.BeginTransaction();
                orders.ForEach(x => db.Insert(x));
                db.CommitTransaction();
            }
        }
    }
}
