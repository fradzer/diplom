﻿using Inc.Analitic.Business.Models;
using Inc.Analitic.Data.Linq2Db;
using Inc.Analitic.Data.Repositories.Interfaces;
using LinqToDB;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inc.Analitic.Data.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        public void DeleteEmployee(Employee employee)
        {
            using (var db = new DbIncAnalitic())
            {
                db.Delete(employee);
            }
        }

        public Employee Find(string name, string hashPassword)
        {
            using (var db = new DbIncAnalitic())
            {
                return db.GetTable<Employee>()
                    .FirstOrDefault(x => 
                        x.Name == name
                        && x.HashPassword == hashPassword);
            }
        }

        public IEnumerable<Employee> GetAll()
        {
            using (var db = new DbIncAnalitic())
            {
                return db.GetTable<Employee>().ToList();
            }
        }

        public void Insert(Employee employee)
        {
            using (var db = new DbIncAnalitic())
            {
                db.Insert(employee);
            }
        }

        public void Update(Employee algorithm)
        {
            throw new NotImplementedException();
        }
    }
}
