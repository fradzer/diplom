﻿using System.Collections.Generic;
using System.Linq;
using Inc.Analitic.Business.Models;
using Inc.Analitic.Data.Linq2Db;
using Inc.Analitic.Data.Repositories.Interfaces;
using LinqToDB;

namespace Inc.Analitic.Data.Repositories
{
    public class ClientRepository : IClientRepository
    {
        public void AddClient(Client newClient)
        {
            using (var db = new DbIncAnalitic())
            {
                db.Insert(newClient);
            }
        }

        public IEnumerable<Client> GetAll()
        {
            using (var db = new DbIncAnalitic())
            {
                return db.GetTable<Client>().ToList();
            }
        }

        public void Update(Client client)
        {
            using (var db = new DbIncAnalitic())
            {
                db.Update(client);
            }
        }
    }
}
