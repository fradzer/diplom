﻿using Inc.Analitic.Business.Models;
using Inc.Analitic.Data.Linq2Db;
using Inc.Analitic.Data.Repositories.Interfaces;
using LinqToDB;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inc.Analitic.Data.Repositories
{
    public class AlgorithmRepository : IAlgorithmRepository
    {
        public IEnumerable<Algorithm> GetAll()
        {
            using (var db = new DbIncAnalitic())
            {
                return db.GetTable<Algorithm>().ToList();
            }
        }

        public void Update(Algorithm algorithm)
        {
            using (var db = new DbIncAnalitic())
            {
                db.Update(algorithm);
            }
        }

        public void Update(List<Algorithm> algorithms)
        {
            using (var db = new DbIncAnalitic())
            {
                db.BeginTransaction();
                algorithms.ForEach(x => db.Update(x));
                db.CommitTransaction();
            }
        }
    }
}
