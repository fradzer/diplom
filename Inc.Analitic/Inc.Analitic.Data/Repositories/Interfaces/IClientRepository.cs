﻿using Inc.Analitic.Business.Models;
using System.Collections.Generic;

namespace Inc.Analitic.Data.Repositories.Interfaces
{
    public interface IClientRepository
    {
        IEnumerable<Client> GetAll();

        void Update(Client client);
        void AddClient(Client newClient);
    }
}
