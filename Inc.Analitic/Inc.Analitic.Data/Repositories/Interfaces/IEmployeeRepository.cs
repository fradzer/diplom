﻿using Inc.Analitic.Business.Models;
using System.Collections.Generic;

namespace Inc.Analitic.Data.Repositories.Interfaces
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> GetAll();

        Employee Find(string name, string hashPassword);

        void Update(Employee employee);

        void Insert(Employee employee);
        void DeleteEmployee(Employee employee);
    }
}
