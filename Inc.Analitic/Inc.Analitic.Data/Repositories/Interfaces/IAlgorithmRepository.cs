﻿using Inc.Analitic.Business.Models;
using System.Collections.Generic;

namespace Inc.Analitic.Data.Repositories.Interfaces
{
    public interface IAlgorithmRepository
    {
        IEnumerable<Algorithm> GetAll();

        void Update(Algorithm algorithm);

        void Update(List<Algorithm> algorithms);
    }
}
