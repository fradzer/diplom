﻿using Inc.Analitic.Business.Models;
using System.Collections;
using System.Collections.Generic;

namespace Inc.Analitic.Data.Repositories.Interfaces
{
    public interface IOrderRepository
    {
        IEnumerable<Order> GetAll();

        void Insert(Order order);

        void Insert(List<Order> orders);

        void Delete(int id);

    }
}
