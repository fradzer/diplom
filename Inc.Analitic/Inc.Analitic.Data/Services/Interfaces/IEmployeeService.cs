﻿using Inc.Analitic.Business.Models;
using System.Collections.Generic;

namespace Inc.Analitic.Data.Services.Interfaces
{
    public interface IEmployeeService
    {
        IEnumerable<Employee> GetAll();
        bool SignIn(string name, string password);
        bool TryAddEmployee(string newEmployeeName, string newEmployeePassword, EmployeeStatus employeeStatus);
        bool TryDeleteEmployee(Employee employee);
    }
}
