﻿using Inc.Analitic.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inc.Analitic.Data.Services.Interfaces
{
    public interface IAlgorithmService
    {
        int GetPrice(int CPA, int CPO, int CPS, int CPL, int CPI);
        
        IEnumerable<Algorithm> GetAll();

        void Update(Algorithm algorithm);

        void Update(List<Algorithm> algorithms);
    }
}
