﻿using Inc.Analitic.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inc.Analitic.Data.Services.Interfaces
{
    public interface IOrderService
    {
        IEnumerable<Order> GetAll();

        IEnumerable<Order> GetAllByClientId(int id);

        void Insert(Order order);

        void Insert(List<Order> orders);

        void Delete(int id);
    }
}
