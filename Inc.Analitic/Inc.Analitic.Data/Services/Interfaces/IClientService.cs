﻿using Inc.Analitic.Business.Models;
using System.Collections.Generic;

namespace Inc.Analitic.Data.Services.Interfaces
{
    public interface IClientService
    {
        IEnumerable<Client> GetAll();

        IEnumerable<Client> GetAllByEmployeeId(int id);
        IEnumerable<Client> GetAllActiveByEmployeeId(int id);

        void Update(Client client);
        bool TryAddClient(string newClientName, string newClientSite, int employeeId);
    }
}
