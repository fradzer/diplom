﻿using Inc.Analitic.Business.Models;
using Inc.Analitic.Data.Repositories.Interfaces;
using Inc.Analitic.Data.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inc.Analitic.Data.Services
{
    public class AlgorithmService : IAlgorithmService
    {
        private readonly IAlgorithmRepository algorithmRepository;

        public AlgorithmService(IAlgorithmRepository algorithmRepository)
        {
            this.algorithmRepository = algorithmRepository;
        }

        public IEnumerable<Algorithm> GetAll()
        {
            return algorithmRepository.GetAll();
        }

        public int GetPrice(int CPA, int CPO, int CPS, int CPL, int CPI)
        {
            var algorithms = algorithmRepository.GetAll();

            var CpaAlgorithm = algorithms.FirstOrDefault(x => x.Name.Equals("cpa", StringComparison.CurrentCultureIgnoreCase));
            var CpoAlgorithm = algorithms.FirstOrDefault(x => x.Name.Equals("cpo", StringComparison.CurrentCultureIgnoreCase));
            var CpsAlgorithm = algorithms.FirstOrDefault(x => x.Name.Equals("cps", StringComparison.CurrentCultureIgnoreCase));
            var CplAlgorithm = algorithms.FirstOrDefault(x => x.Name.Equals("cpl", StringComparison.CurrentCultureIgnoreCase));
            var CpiAlgorithm = algorithms.FirstOrDefault(x => x.Name.Equals("cpi", StringComparison.CurrentCultureIgnoreCase));

            var result = CPA * CpaAlgorithm.Price
                       + CPO * CpoAlgorithm.Price
                       + CPS * CpsAlgorithm.Price
                       + CPL * CplAlgorithm.Price
                       + CPI * CpiAlgorithm.Price;

            return (int)result;
        }

        public void Update(Algorithm algorithm)
        {
            algorithmRepository.Update(algorithm);
        }

        public void Update(List<Algorithm> algorithms)
        {
            algorithmRepository.Update(algorithms);
        }
    }
}
