﻿using System.Collections.Generic;
using System.Linq;
using Inc.Analitic.Business.Models;
using Inc.Analitic.Data.Repositories.Interfaces;
using Inc.Analitic.Data.Services.Interfaces;

namespace Inc.Analitic.Data.Services
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository clientRepository;

        public ClientService(IClientRepository clientRepository)
        {
            this.clientRepository = clientRepository;
        }

        public IEnumerable<Client> GetAll()
        {
            return clientRepository.GetAll();
        }

        public IEnumerable<Client> GetAllActiveByEmployeeId(int id)
        {
            return clientRepository.GetAll().Where(x => x.EmployeeId == id && x.Status == ClientStatus.Active);
        }

        public IEnumerable<Client> GetAllByEmployeeId(int id)
        {
            return clientRepository.GetAll().Where(x => x.EmployeeId == id);
        }

        public bool TryAddClient(string newClientName, string newClientSite, int employeeId)
        {
            if (clientRepository.GetAll().Any(x => x.CompanyName.Equals(newClientName, System.StringComparison.CurrentCultureIgnoreCase)))
            {
                return false;
            }
            else
            {
                var newClient = new Client()
                {
                    CompanyName = newClientName,
                    Site = newClientSite,
                    EmployeeId = employeeId,
                    Status = ClientStatus.Active
                };

                clientRepository.AddClient(newClient);
                return true;
            }
        }

        public void Update(Client client)
        {
            clientRepository.Update(client);
        }
    }
}
