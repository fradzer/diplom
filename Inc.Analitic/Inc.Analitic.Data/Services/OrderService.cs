﻿using System.Collections.Generic;
using System.Linq;
using Inc.Analitic.Business.Models;
using Inc.Analitic.Data.Repositories.Interfaces;
using Inc.Analitic.Data.Services.Interfaces;

namespace Inc.Analitic.Data.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }

        public void Delete(int id)
        {
            orderRepository.Delete(id);
        }

        public IEnumerable<Order> GetAll()
        {
            return orderRepository.GetAll();
        }

        public IEnumerable<Order> GetAllByClientId(int id)
        {
            return orderRepository.GetAll().Where(x => x.ClientId == id);
        }

        public void Insert(Order order)
        {
            orderRepository.Insert(order);
        }

        public void Insert(List<Order> orders)
        {
            orderRepository.Insert(orders);
        }
    }
}
