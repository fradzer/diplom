﻿using Inc.Analitic.Business.Models;
using Inc.Analitic.Data.Repositories.Interfaces;
using Inc.Analitic.Data.Services.Interfaces;
using Inc.Analitic.Data.Session;
using Inc.Analitic.Data.Utils;
using System.Collections.Generic;
using System.Linq;

namespace Inc.Analitic.Data.Services
{
    public class EmployeeService: IEmployeeService
    {
        private readonly IEmployeeRepository employeeRepository;
        private readonly IClientRepository clientRepository;

        public EmployeeService(IEmployeeRepository employeeRepository, IClientRepository clientRepository)
        {
            this.employeeRepository = employeeRepository;
            this.clientRepository = clientRepository;
        }

        public IEnumerable<Employee> GetAll()
        {
            return employeeRepository.GetAll();
        }

        public bool SignIn(string name, string password)
        {
            var hashPassword = HashUtil.CreateMD5(password);
            var employee = employeeRepository.Find(name, hashPassword);
            if(employee != null)
            {
                CurrentEmployee.Id = employee.Id;
                CurrentEmployee.Name = employee.Name;
                CurrentEmployee.Status = employee.Status;
                return true;
            }
            return false;
        }

        public bool TryAddEmployee(string newEmployeeName, string newEmployeePassword, EmployeeStatus employeeStatus)
        {
            if (employeeRepository.GetAll()
                .Any(x => x.Name.Equals(newEmployeeName, System.StringComparison.CurrentCultureIgnoreCase)))
            {
                return false;
            }

            var newEmployee = new Employee()
            {
                Name = newEmployeeName,
                HashPassword = HashUtil.CreateMD5(newEmployeePassword),
                Status = employeeStatus
            };

            employeeRepository.Insert(newEmployee);
            return true;
        }

        public bool TryDeleteEmployee(Employee employee)
        {
            if(clientRepository.GetAll().Any(x => x.EmployeeId == employee.Id && x.Status == ClientStatus.Active))
            {
                return false;
            }
            else
            {
                employeeRepository.DeleteEmployee(employee);
                return true;
            }
        }
    }
}
