﻿using Inc.Analitic.Business.Models;

namespace Inc.Analitic.Data.Session
{
    public static class CurrentEmployee
    {
        public static int Id { get; set; }

        public static string Name { get; set; }

        public static EmployeeStatus Status { get; set; }

        public static bool IsAdmin()
        {
            return Status == EmployeeStatus.Admin;
        }
    }
}
