﻿using LinqToDB.Mapping;
using System;

namespace Inc.Analitic.Business.Models
{
    [Table("Orders")]
    public class Order
    {
        [PrimaryKey, Identity]
        public int Id { get; set; }

        [Column(Name = "CreateDate"), NotNull]
        public DateTimeOffset CreateDate { get; set; }

        [Column(Name = "EmployeeId"), NotNull]
        public int EmployeeId { get; set; }

        [Column(Name = "ClientId"), NotNull]
        public int ClientId { get; set; }

        [Column(Name = "CPA"), NotNull]
        public int CPA { get; set; }

        [Column(Name = "CPO"), NotNull]
        public int CPO { get; set; }

        [Column(Name = "CPS"), NotNull]
        public int CPS { get; set; }

        [Column(Name = "CPL"), NotNull]
        public int CPL { get; set; }

        [Column(Name = "CPI"), NotNull]
        public int CPI { get; set; }

        [Column(Name = "Price"), NotNull]
        public decimal Price { get; set; }
    }
}
