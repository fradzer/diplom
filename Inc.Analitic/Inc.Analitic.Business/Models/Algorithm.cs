﻿using LinqToDB.Mapping;

namespace Inc.Analitic.Business.Models
{
    [Table("Algorithms")]
    public class Algorithm
    {
        [PrimaryKey, Identity]
        public int Id { get; set; }

        [Column(Name = "Name"), NotNull]
        public string Name { get; set; }

        [Column(Name = "Price"), NotNull]
        public decimal Price { get; set; }
    }
}
