﻿using LinqToDB.Mapping;
using System.ComponentModel;

namespace Inc.Analitic.Business.Models
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum EmployeeStatus
    {
        [Description("Специалист")]
        Optimizer = 1,
        [Description("Администратор")]
        Admin = 2
    }

    [Table("Employees")]
    public class Employee
    {
        [PrimaryKey, Identity]
        public int Id { get; set; }

        [Column(Name = "Name"), NotNull]
        public string Name { get; set; }

        [Column(Name = "HashPassword"), NotNull]
        public string HashPassword { get; set; }

        [Column(Name = "Status"), NotNull]
        public EmployeeStatus Status { get; set; }
    }
}