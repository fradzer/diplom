﻿namespace PerfomanceTest.Models
{
    class ChartCollection : System.Collections.ObjectModel.Collection<RequestsStatistic>
    {
        public ChartCollection()
        {
            Add(new RequestsStatistic { Name = "Successes requests", Amount = 1 });
            Add(new RequestsStatistic { Name = "Failed requests", Amount = 1 });
        }
    }    
}
