﻿using LinqToDB.Mapping;
using System.ComponentModel;

namespace Inc.Analitic.Business.Models
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum ClientStatus
    {
        [Description("Активный")]
        Active = 1,
        [Description("Отмененный")]
        Canceled = 2,
        [Description("Закончченый")]
        Finished = 3
    }

    [Table("Clients")]
    public class Client
    {
        [PrimaryKey, Identity]
        public int Id { get; set; }

        [Column(Name = "CompanyName"), NotNull]
        public string CompanyName { get; set; }

        [Column(Name = "Status"), NotNull]
        public ClientStatus Status { get; set; }

        [Column(Name = "Capital"), NotNull]
        public decimal Capital { get; set; }

        [Column(Name = "Site"), NotNull]
        public string Site { get; set; }

        [Column(Name = "EmployeeId"), NotNull]
        public int EmployeeId { get; set; }
    }
}
